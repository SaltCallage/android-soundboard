# SoundBoard Android Studio Tutorial

In order to do this tutorial you will have to download and install **Android Studio** (https://developer.android.com/studio/). The installer alone is 790 megs so get yourself ready for some downloading.

### Creating a new project in Android Studio
Open Android Studio, a "Welcome to Android Studio" screen will open up.
Select 'Start a new Android Studio Project'x`

On the next screen, type a project name with something like 'Android Soundboard Tutorial'. You don't need to worry about the company name, make sure the project location is somewhere acceptable.
Choose API 14 of Android and on the screen after that Choose Empty activity, leave the name default as MainActivity.
Once you click finish, it will open up a fresh project and two files: activity_main.xml, and MainActivity.java.

### Adding our button image into the project
```
NOTE ABOUT ADDING FILES: Keep your filenames simple characters and lowercase.
Android Studio will probably complain about it if you don't.
```
On the left you should see your project explorer, starting with a folder called "app". If you don't see this, there should be a button labeled 'project' you can press to pull out the project hierarchy.

Expand the tree to expose the following folder:
app > res > drawable
Right click on drawable and select 'show in explorer'. Paste the image files you want to use in your app in the "drawable" folder.

### Adding the sound files into the project
```
NOTE ABOUT ADDING FILES: Keep your filenames simple characters and lowercase.
Android Studio will probably complain about it if you don't.
```
Return to Android Studio and in the Project Hierarchy right click on the 'res' folder and select ```new > directory```, name it raw. Place your sound files into this directory, you can use the techniques in the step above for accessing the directory on the filesystem.

### Adding a Button to the project
Select the activity_main.xml file, it should show you two android style mockup screens and "Hello World!" written across the middle of the screen.

If you're seeing a single screen and a bunch of code, switch from Text to Design view using the tabs at the bottom left of the screen.

To the left of your designer view there should be a palette which allows you to select different UI elements to add to the screen. It should be on 'Common' elements by default so click and drag a button onto your screen.
### Customizing the button
Turn your attention to the **Attributes panel** on the right side. This is where we can customize the visual properties of the button and if we wanted, set an event handler.

Go into the attributes for the button and remove the Text from the "TextView" attribute. Click on the three dots next to the **background** option in the "Button" attribute above it.

 The window that pops up will let you pick the image you want, click on the "project" label to open up the tray holding all of the images contained in your project, select the image you want and click OK.
 
 You'll want to tweak the button's size and position to suit your needs. Once you're done, pop back into the text view to see the button block.
 ### Adding your sound to an OnClick event
 Just like in JavaScript, we're going to create some event handlers when this Activity is started that handle the buttons behavior.
 
 For this, we have to turn to the MainActivity.java file that should have opened when the project was created. Within the java file you will find an already generated onCreate function with a couple of lines of code, our code is going to begin just after those lines of code but still within the onCreate function.
 Type the following code:
 ```java
final MediaPlayer mp1 = MediaPlayer.create(this, R.raw.axe);
 
 Button firstButton = findViewById(R.id.button);
 ```
 The first expression is creating a media player for my first button's sound file which is called 'axe.wav' note that as soon as you type `R.raw.` you should see the files you've placed in the raw folder for auto completion.
 
 The second expression creates a variable for a Button called firstButton. findViewById() is a function that you can use to find elements in your Activity, *(think getElementById() in javascript ).
Now to add an event handler to the button:
```java
firstButton.setConClickListener(
new View.OnClickListener() {  
    @Override  
  public void onClick(View v) {  
          mp1.start();
    }  
});
```
### Test your application!
Guess what, that's pretty much it, you should be able to mash the green play button up above.
When you do that, it's going to ask you to set up a virtual machine to emulate your program on. You will also have to download said virtual machine. I chose build 26 (Oreo) and it worked just fine for this exercise. If you remember, we chose an earlier version of Android with a high compatibility rating for this reason.

## Assignment: Create Your own Soundboard and merge it into the repository.
You are tasked with creating an android studio soundboard project with **at least four buttons.** Use git to clone the repository, add a folder with your name on it in the submissions folder and place your android studio project and a built apk in the directory.
### Adding additional buttons to the application
In order to add new buttons to your program, you basically just need to repeat the tutorial for every button you want to add.
1) Place your button in the designer, assign the background and text
2) In the .java file create the following elements
- A Button variable which finds the button
- A MediaPlayer variable which points to the file
- The onClick listener which calls the MediaPlayer.start() method.
 

