package com.example.mike.soundboardtutorial;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //For the sake of filesize, I made them all point to the same file
        final MediaPlayer mp1 = MediaPlayer.create(this, R.raw.axe);
        final MediaPlayer mp2 = MediaPlayer.create(this, R.raw.axe);
        final MediaPlayer mp3 = MediaPlayer.create(this, R.raw.axe);
        final MediaPlayer mp4 = MediaPlayer.create(this, R.raw.axe);


        Button firstButton = findViewById(R.id.button);
        Button secondButton = findViewById(R.id.button2);
        Button thirdButton = findViewById(R.id.button3);
        Button fourthButton = findViewById(R.id.button4);

        firstButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp1.start();
            }
        });

        secondButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp2.start();
            }
        });

        thirdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp3.start();
            }
        });

        fourthButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp4.start();
            }
        });

    }
}
